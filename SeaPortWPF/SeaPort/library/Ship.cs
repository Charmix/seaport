﻿using System;
using System.Windows.Controls;
using System.Threading;
using System.Windows.Media.Imaging;
namespace SeaPort
{
	public class Ship:IComparable<Ship>
	{
		//Приватные поля
		private DateTime _ArrivalTime;
		private TimeSpan _DurationUnloadTime;
		private DateTime _StartUnloadTime;
		private DateTimeFunctions DTFunctions = new DateTimeFunctions ();
		private Random random = new Random();
		private int _CargoType;
		private string[] _CargoTypeStringArray= {"Сыпучий", "Жидкий", "Контейнеры"};
		//Доступные поля
        public Canvas Outlook;
        public Position MyPosition;
		public string Name;
		public int CargoWeight;

		public int Step; //0 - не прибыл; 1 - прибыл, в очереди на разгрузку; 2 - разгружается; 3 - закончил разгрузку.
		public DateTime ArrivalTime {
			get { return _ArrivalTime; }
			set { _ArrivalTime=value;}
		}
		public TimeSpan DurationUnloadTime {
			get { return _DurationUnloadTime; }
		}
		public DateTime StartUnloadTime {
			get { return _StartUnloadTime; }
			set { _StartUnloadTime = value;}
		}
		public int CargoType {
			get { return _CargoType; }
		}
		public string CargoTypeString {
			get { return _CargoTypeStringArray[_CargoType]; }
		}

		//Методы
		public void AddToArrivalTime(TimeSpan time) {
			_ArrivalTime=_ArrivalTime.Add(time);
		}
		public void UpStep() {
			this.Step++;
		}
		public void AddToDurationUnloadTime(TimeSpan time) {
			_DurationUnloadTime = _DurationUnloadTime+time;
		}
		public TimeSpan GetWaitingTime() {
			 TimeSpan ret=_StartUnloadTime - _ArrivalTime;
			 if (ret==new TimeSpan(0,0,0,10)) {//Kostql
				ret=new TimeSpan(0,0,0,0);
			 }
			return ret;
		}
		public string ToPrint() {
			string str ="Название: "+this.Name+" ("+this.CargoType+" "+this.CargoTypeString+")";
			str += "\nВремя прибытия: " + this._ArrivalTime;
			str += "\nВремя ожидания: " + this.GetWaitingTime();
			str += "\nПродолжительность разгрузки: " + this._DurationUnloadTime;
			str += "\n";
			return str;
		}
		public static bool operator <(Ship obj1, Ship obj2) {
			return (obj1.ArrivalTime < obj2.ArrivalTime);
		}
		public static bool operator >(Ship obj1, Ship obj2) {
			return (obj1.ArrivalTime > obj2.ArrivalTime);
		}
		public int CompareTo(Ship obj){
			if (this._ArrivalTime > obj._ArrivalTime)
				return 1;
			if (this._ArrivalTime < obj._ArrivalTime)
				return -1;
			else
				return 0;
		}
		//Конструкторы
		public Ship (string name, DateTime ArrivalTime, TimeSpan DurationUnloadTime, int CargoType) {
			_ArrivalTime = ArrivalTime;
			_DurationUnloadTime = DurationUnloadTime;
			Name=name;
			_CargoType = CargoType;
			Step = 0;
			CargoWeight = random.Next(2000, 5000);
		}
	}

}

