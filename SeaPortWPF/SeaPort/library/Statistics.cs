﻿using System;
using System.Collections.Generic;

namespace SeaPort
{
	public class Statistics
	{
		//Приватные поля
		private Port MyPort;
		private Schedule MySchedule;

		//Методы
		public int GetTheNumberOfShips() {
			return MySchedule.AllShips.Count;
		}
		public TimeSpan GetMidWaitingTime() {
			TimeSpan midWaitingTime = new TimeSpan(GetAllWaitingTime().Ticks / MySchedule.AllShips.Count);
			return midWaitingTime;
		}
		public TimeSpan GetMaxWaitingTime() {
			TimeSpan maxWaitingTime=new TimeSpan();
			foreach (Ship element in MySchedule.AllShips) {
				if (maxWaitingTime < element.GetWaitingTime ()) {
					maxWaitingTime = element.GetWaitingTime ();
				}
			}
			return maxWaitingTime;
		}
		public int GetMidLengthOfQueue() {
			int queue_count = 0;
			foreach (HarborCrane crane in MyPort.PortCranes) {
				queue_count += crane.MaxQueue;
			}
			return Convert.ToInt32(Math.Round ((double) (queue_count / MyPort.PortCranes.Count)));
		}
		public TimeSpan GetAllWaitingTime() {
			TimeSpan allWaitingTime=new TimeSpan();
			foreach (Ship element in MySchedule.AllShips) {
				allWaitingTime = allWaitingTime+element.GetWaitingTime ();
			}
			return allWaitingTime;
		}
		public double GetTotalPenalties() {
			return GetAllWaitingTime ().Hours * MyPort.PenaltyForOneHour;
		}
		public string GetStatsInformation() {
			string str="";
			str += "Число разгруженный кораблей: " + GetTheNumberOfShips() + "\n";
			str += "Средняя длина очереди: " + GetMidLengthOfQueue() + "\n";
			str += "Среднее время ожидания: " + GetMidWaitingTime() + "\n";
			str += "Максимальное время ожидания: " + GetMaxWaitingTime() + "\n";
			str += "Общая сумма штрафа: " + GetTotalPenalties() + "\n";
			return str;
		}
		public string GetScheduleInformation() {
			string str="";
			foreach (Ship element in MySchedule.AllShips) {
				str+=element.ToPrint()+"\n";
			}
			return str;
		}
		//Конструктор
		public Statistics () {
			MySchedule = Schedule.getInstance ();
			MyPort = Port.getInstance ();
		}
	}
}

