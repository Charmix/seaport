﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SeaPort
{
    public class Constants
    {
        public const double SpawnShipYCoord = -30;
        public const double ShipHeight = 40;
        public const double ShipWidht = 15;
        public static double StartShipXCoord = 10;
        public const double StartShipYCoord = 50;

        public static double StartCraneXCoord = 30;
        public const double StartCraneYCoord = 410;
        public const double deltaCraneXCoord = 120;
    }
}
