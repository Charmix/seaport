﻿using System;

namespace SeaPort
{
	public interface Observer
	{
		void TimeUpdate ();
		void JoAction(string mes, int id);
	} 
}

