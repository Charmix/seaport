﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Controls;
namespace SeaPort
{
	public class Port
	{
		//Приватные поля
			private int[] _NumberOfCranes;
			private TimeSpan[] _DeviationFromSchedule;
			private TimeSpan[] _DeviationFromUnloadTime;
			private TimeSpan _StepSimulation;
			private TimeSpan _PeriodSimulation;
			private List<HarborCrane> _PortCranes;
			private List<String> _History;
			private Random rnd;
			private Schedule MySchedule;
			  private MainWindow.CraneHandler CraneOutlook;
			  private MainWindow.ShipMoveHandler ShipMover;
			  private MainWindow.ShipMoveHandler ShipSpawner;
			  private MainWindow.ShipHandler RemoveShip;
        //Поле хранящее экземпляр Port
        private static Port uniqueInstance;

		//Доступные поля
		public double PenaltyForOneHour;
		public List<String> History {
			get { return _History; }
		}
		public int[] NumberOfCranes {
			get { return _NumberOfCranes; }
		}
		public TimeSpan[] DeviationFromSchedule {
			get { return _DeviationFromSchedule; }
		}
		public TimeSpan[] DeviationFromUnloadTime {
			get { return _DeviationFromUnloadTime; }
		}
		public TimeSpan StepSimulation {
			get { return _StepSimulation; }
		}
		public TimeSpan PeriodSimulation {
			get { return _PeriodSimulation; }
		}
		public List<HarborCrane> PortCranes {
            get { return _PortCranes; }
      }

		//Методы
		public List<HarborCrane> CreateCranes(int[] numberOfCranes) {
            List<HarborCrane> cranes = new List<HarborCrane>();
            for (int i = 0; i < numberOfCranes.Length; i++)
                for (int j = 0; j < numberOfCranes[i]; j++)
                {
                    var createdCrane = new HarborCrane(i,ShipMover,ShipSpawner,RemoveShip);
                    cranes.Add(createdCrane);
                    CraneOutlook.Invoke(createdCrane);
                }
            return cranes;
        }
		public void AddArrivedShipToCrane(Ship arrivedShip) {
            HarborCrane craneForShip = FindCrane(arrivedShip.CargoType);
				craneForShip.AddShipToQueue(arrivedShip);
            
            var spawnPosition = new Position();
            spawnPosition.SetPosition(craneForShip.MyPosition.X,Constants.SpawnShipYCoord);
            ShipSpawner.Invoke(arrivedShip, spawnPosition,TimeSpan.FromSeconds(0));

            int index =craneForShip.FindShipInQueue(arrivedShip);
				//AddToHistory (craneForShip.ToPrintQueue());
        }
		public HarborCrane FindCrane(int typeOfCargo) {
				HarborCrane freeCrane=PortCranes[0];
				for (int i = 0; i < PortCranes.Count; i++)
					if (PortCranes[i].CargoType == typeOfCargo){
						freeCrane = PortCranes[i];
					}
            TimeSpan freeCraneTime = freeCrane.UploadQueueTime();
            for (int i = 0; i < PortCranes.Count; i++)
                if (PortCranes[i].CargoType == typeOfCargo){
                    TimeSpan craneTime = PortCranes[i].UploadQueueTime();
					if (craneTime < freeCraneTime) {
                        freeCrane = PortCranes[i];
                        freeCraneTime = freeCrane.UploadQueueTime();
                    }
                }
            return freeCrane;
        }
			public void AddToHistory(string Message) {
				_History.Add (Message);
				JoTimer myJoTimer=JoTimer.getInstance();
				myJoTimer.NeedAction(Message, 1);
			}
			public TimeSpan GenerateDeviationFromUnloadTime() {
				TimeSpan time=new TimeSpan();
				double dif=DeviationFromUnloadTime[1].TotalMinutes;
			   int k=rnd.Next(0, Convert.ToInt32(dif));
				return time.Add(TimeSpan.FromMinutes(k));
			}
			public TimeSpan GenerateDeviationFromArrivedTime()
			{
				//TimeSpan time = new TimeSpan();
				double dif =  DeviationFromSchedule[0].TotalMinutes;
				double dif2 =  DeviationFromSchedule[1].TotalMinutes;
				//int k = rnd.Next(Convert.ToInt32(dif), Convert.ToInt32(dif2));
				TimeSpan one = new TimeSpan(0, 53, 3);
				return one;
				//return time.Add(TimeSpan.FromMinutes(k));
			}
        //Методы создания экземпляра Port
			public static Port getInstance() {
            return uniqueInstance;
        }
        //Данный метод используется только при создании
			public static Port getInstance(TimeSpan stepSimulation, TimeSpan periodSimulation, int[] Cranes, TimeSpan[] DeviationFromSchedule, TimeSpan[] DeviationFromUnloadTime, double PortPenalty,
            MainWindow.CraneHandler craneCreator, MainWindow.ShipMoveHandler shipMover, MainWindow.ShipMoveHandler shipSpawner, MainWindow.ShipHandler removeShip)
				{
            if (uniqueInstance == null)
                uniqueInstance = new Port(stepSimulation, periodSimulation, Cranes, DeviationFromSchedule, DeviationFromUnloadTime, PortPenalty,
                    craneCreator,shipMover,shipSpawner, removeShip);
            return uniqueInstance;
        }

		//Конструктор (Приватный для Одиночки)
		private Port (TimeSpan stepSimulation, TimeSpan periodSimulation, int[] Cranes, TimeSpan[] DeviationFromSchedule, TimeSpan[] DeviationFromUnloadTime, double PortPenalty, 
            MainWindow.CraneHandler craneCreator, MainWindow.ShipMoveHandler shipMover,MainWindow.ShipMoveHandler shipSpawner,MainWindow.ShipHandler removeShip) {
            CraneOutlook = craneCreator;
            ShipMover = shipMover;
            ShipSpawner = shipSpawner;
            RemoveShip = removeShip;
            _StepSimulation = stepSimulation;
				_PeriodSimulation = periodSimulation;
				_NumberOfCranes = Cranes;
				_DeviationFromSchedule = DeviationFromSchedule;
				_DeviationFromUnloadTime = DeviationFromUnloadTime;
            _PortCranes = CreateCranes(_NumberOfCranes);
				PenaltyForOneHour = PortPenalty;
				_History = new List<String> ();
				rnd = new Random();
		}
	}
}

