﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Timers;
using System.Windows.Controls;
using System.Threading;

namespace SeaPort
{
	public class Schedule:Observer
	{
		//Поля
		public List<Ship> AllShips = new List<Ship> (); //Только здесь хранится весь список кораблей 
		private JoTimer MyJoTimer;
		private Port MyPort;
		private Random rnd;
		private static Schedule uniqueInstance;

        private MainWindow.ShipHandler ArriveReaction;
		//Методы
		private void Sort() {
			AllShips.Sort ();
		}
		void Observer.TimeUpdate() {
			//Время обновилось
			Ship cur_ship;
			//Сделал циклом, чтобы можно было реализовать прибытие кораблей в одной время.
			while ((cur_ship = AllShips.Find (x => x.Step == 0)) != default(Ship) && MyJoTimer.CheckThisTime (cur_ship.ArrivalTime)) {
					ShipArrive (cur_ship);
			}
			if (this.AllShips.Find(x => x.Step <= 2) == default(Ship)) {
				MyJoTimer.NeedAction("Все корабли закончили разгрузку", 2);
			} 
		}
		void Observer.JoAction(string mes, int id)
		{

		}
		public void ShipArrive(Ship arrivedShip) {
				arrivedShip.UpStep ();
				//MyPort.AddToHistory(arrivedShip.Step.ToString());
            ArriveReaction.Invoke(arrivedShip);
				//Отлклонения от времени разгрузку. Примерно 25% шанс задежрать разгрузку
				int kf = rnd.Next(0, 3);
				TimeSpan time = MyPort.GenerateDeviationFromUnloadTime();
				if (kf == 2) {
					arrivedShip.AddToDurationUnloadTime(time);
					MyPort.AddToHistory("Разгрузка корабля " + arrivedShip.Name + " задерживается на " + time);
				}
				MyPort.AddArrivedShipToCrane(arrivedShip);
        }
		 public void DeviationFromSchedule() {
		   /* int ind=rnd.Next(0, AllShips.Count);
			 TimeSpan time;
			 while (AllShips[ind].ArrivalTime.Add( time = MyPort.GenerateDeviationFromArrivedTime()) > MyJoTimer.GetTime()) {
				AllShips[ind].AddToArrivalTime(time);
			 }
			
			//MyPort.AddToHistory(AllShips[ind].Name + " опаздает на " + time);
			*/
		 } 
		//Данный метод используется только при создании
        public static Schedule getInstance(MainWindow.ShipHandler Reaction,string filename)
        {
            if (uniqueInstance == null)
                uniqueInstance = new Schedule(Reaction,filename);
            return uniqueInstance;
        }
		public static Schedule getInstance() {
            if (uniqueInstance == null)
                uniqueInstance = new Schedule(null,null);
			return uniqueInstance;
		}
		//Конструкторы
		private Schedule (MainWindow.ShipHandler s, string filename2){
			rnd = new Random();
			MyPort = Port.getInstance (); //Получили объект порта(паттерн Одиночка)
          ArriveReaction = s;
			string line = "";
			string[] del = new string[]{ " @ " };
				using (StreamReader str2 = new StreamReader (filename2)) {
					while((line = str2.ReadLine()) != null) {
						// Килт @ 14.11.2016 6:50:30 @ 01:02:50:00 @ 1
						string[] words = line.Split (del, StringSplitOptions.RemoveEmptyEntries);
						AllShips.Add(new Ship (words[0], DateTime.Parse(words[1]),  TimeSpan.Parse(words[2]), int.Parse(words[3])));
					}
				}	
			AllShips.Sort ();
			MyJoTimer = JoTimer.getInstance (AllShips [0].ArrivalTime.AddMinutes (-1), new List<Observer>(MyPort.PortCranes));
			MyJoTimer.Start ();
		}
	}
}

