﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SeaPort
{
    public class Position
    {
        private double _X;
        private double _Y;

        public double X
        {
            get { return _X; }
        }
        public double Y
        {
            get { return _Y; }
        }

        public void SetPosition(double x, double y)
        {
            _X = x;
            _Y = y;
        }
        public Position()
        {
            _X = 0;
            _Y = 0;
        }
    }
}
