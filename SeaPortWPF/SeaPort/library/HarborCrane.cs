﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
namespace SeaPort
{
	public class HarborCrane:Observer
    {
		//Поля
			private int _CargoType;
			private string _CraneId;
			private Queue<Ship> _UnloadQueue; 
			private int _MaxQueue=0;
			private Ship _UnloadShip;
			private JoTimer MyJoTimer;
			private Port MyPort;
			private List<Position> PositionsInQueue = new List<Position>();
			  private MainWindow.ShipMoveHandler ShipMover;
			  private MainWindow.ShipMoveHandler SpawnShip;
			  private MainWindow.ShipHandler RotateShip;

        public Position MyPosition;
        public Canvas Outlook;

		public int CargoType {
            get { return _CargoType; }
        }
		public int MaxQueue {
			get { return _MaxQueue; }
		}
		public Ship UnloadShip {
			get { return _UnloadShip; }
		}

		//Методы
        public void GeneratePositions()
        {
            double yCoord = MyPosition.Y-15;
            double xCoord = MyPosition.X;
            double freeSpace = 10;
            double ShipHeight = Constants.ShipHeight;
            PositionsInQueue = new List<Position>();
            while(yCoord>0)
            {
                var pos = new Position();
                var NewYCoord = yCoord - freeSpace - ShipHeight;
                pos.SetPosition(xCoord, NewYCoord);
                PositionsInQueue.Add(pos);
                yCoord = NewYCoord;
            }
            
        }
		public string ToPrintQueue() {
			string result = "Очередь крана: ";
			foreach (var ship in _UnloadQueue) {
				result += ship.Name+" ";
            }
            return result;
        }
        public int FindShipInQueue(Ship ship)
        {
            var k =_UnloadQueue.ToArray<Ship>();
            for (int index = 0; index < k.Length; index++)
                if (ship == k[index])
                    return index;
            return -1;
        }
        public Position GetPosition(int index) {
                return PositionsInQueue[index];
        }
        public void MoveShipsInQueue()
        {
            Ship[] shipsQueue = _UnloadQueue.ToArray<Ship>();
            for(int i=0; i<_UnloadQueue.Count;i++)
            {
                var ship = shipsQueue[i];
                ShipMover.Invoke(ship,PositionsInQueue[i],TimeSpan.FromSeconds(0));
                ship.MyPosition = PositionsInQueue[i];
            }
        }
        public void MoveShipOutOfQueue(Ship ship)
        {
            var RotationPosition = new Position();
            var RemovePosition = new Position();
            RotationPosition.SetPosition(ship.MyPosition.X , ship.MyPosition.Y + 20);
            RemovePosition.SetPosition(ship.MyPosition.X, Constants.SpawnShipYCoord);
            ShipMover.Invoke(ship, RotationPosition,TimeSpan.FromSeconds(0));
            RotateShip.Invoke(ship);
            ShipMover.Invoke(ship, RemovePosition,TimeSpan.FromSeconds(2));
            
        }
		public void AddShipToQueue(Ship ship) {
				_UnloadQueue.Enqueue (ship);
            var SpawnPosition = new Position();
            SpawnPosition.SetPosition(MyPosition.X,Constants.SpawnShipYCoord);
            SpawnShip.Invoke(ship,SpawnPosition,TimeSpan.FromSeconds(0));
            MoveShipsInQueue();
				if (_UnloadQueue.Count > _MaxQueue && _UnloadShip != null) {
					_MaxQueue = _UnloadQueue.Count;
				}
       }
		public TimeSpan UploadQueueTime() {
            TimeSpan tspan = new TimeSpan();
            foreach (var ship in _UnloadQueue)
                tspan += ship.DurationUnloadTime;
            return tspan;
        }
		public string generateId() {
			return Guid.NewGuid().ToString("N");
		}
		void Observer.JoAction(string mes, int id) {}
		void Observer.TimeUpdate() {
			MyPort = Port.getInstance ();
			MyJoTimer = JoTimer.getInstance ();
			//Время обновилось
			if (_UnloadShip == null) {
				if (_UnloadQueue.Count != 0) {
					//Если очередь на разгрузку не пуста и нету текущего разгружаемого корабля, то отправляем его на разгрузку.
					//_UnloadShip = _UnloadQueue.Dequeue ();
               _UnloadShip = _UnloadQueue.Peek();
					_UnloadShip.UpStep ();
					_UnloadShip.StartUnloadTime = MyJoTimer.GetTime ();
				}
			} else {
				if (_UnloadShip.StartUnloadTime.Add(_UnloadShip.DurationUnloadTime) == MyJoTimer.GetTime ()) {
					MyPort.AddToHistory ("Корабль "+_UnloadShip.Name+" закончил разгрузку "+"(" +MyJoTimer.GetTime()+")");
					_UnloadShip.UpStep ();
                    MoveShipOutOfQueue(_UnloadShip);
                    _UnloadQueue.Dequeue();
                    MoveShipsInQueue();
					_UnloadShip = null;
				}
			}
		}

		//Конструкторы
        public HarborCrane(int cargoType, MainWindow.ShipMoveHandler shipMover, MainWindow.ShipMoveHandler shipSpawner,MainWindow.ShipHandler removeShip)
        {
                ShipMover = shipMover;
                SpawnShip = shipSpawner;
                RotateShip = removeShip;
                _CargoType = cargoType;
                _UnloadQueue = new Queue<Ship>();
                MyPosition = new Position();
                _CraneId = generateId();
                _UnloadShip = null;
                var x = Constants.StartCraneXCoord;
                var y = Constants.StartCraneYCoord;
                Constants.StartCraneXCoord += Constants.deltaCraneXCoord;
                MyPosition.SetPosition(x, y);
                GeneratePositions();
        }
    }
}
