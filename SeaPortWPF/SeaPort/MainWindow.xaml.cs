﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Windows.Media.Animation;
using System.IO;

namespace SeaPort
{
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window, Observer
	{
		JoTimer MyJoTimer; 
		Schedule MySchedule;
		Port MyPort;
		public delegate void ShipHandler(Ship ship);
		public delegate void CraneHandler(HarborCrane crane);
		public delegate void ShipMoveHandler(Ship ship, Position pos, TimeSpan tspan);
		public MainWindow() {
			InitializeComponent();
            Canvas.SetZIndex(Start, 5);
            Canvas.SetZIndex(FileNameBox, 5);
            Canvas.SetZIndex(TimeLabel, 5);
            Canvas.SetZIndex(MenuBackIm, 5);
            Canvas.SetZIndex(ShipLabel, 5);
            Canvas.SetZIndex(Stats_but, 5);
				Stats_but.Visibility=Visibility.Hidden;
				Show_schedule.Visibility = Visibility.Hidden;
				Information_block.Visibility= Visibility.Hidden;
				History_but.Visibility = Visibility.Hidden;
		}
		private void Start_Click(object sender, RoutedEventArgs e) {
				int[] portCranes=new int[3];
				double portPenalty=0;
				TimeSpan[] portDevitationFromSchedule=new TimeSpan[2];
				TimeSpan[] portDevitationFromUnloadTime = new TimeSpan[2];
            bool CorrectParametersFile = false;
				bool CorrectScheduleFile=false;
				string line = "";
				string[] del = new string[] { " @ " };
				string[] del2 = new string[] { "," };
           try {
                using (StreamReader str = new StreamReader (FileNameBox.Text))
                {
							//ЧТЕНИЕ РАСПИСАНИЯ
							string errorMessage = "Неверный формат содержимого файла \n Содержание должно иметь вид: \n Название_корабля @ Дата_прибытия(ДД.ММ.ГГГГ ЧЧ:ММ:СС) @ Время_разгрузки(ЧЧ:ММ:СС:ММ) @ Тип_груза(0 - Сыпучий, 1 - Жидкий, 2 - Контейнеры)";
							while ((line = str.ReadLine()) != null) {
                        string[] words = line.Split(del, StringSplitOptions.RemoveEmptyEntries);
                        if (words.Length != 4) {
                            Exception ex = new Exception(errorMessage);
                            throw ex;
                        }
								if (words[0].Length > 10)
								{
									Exception ex = new Exception("Слишком большое имя для корабля, введите другое");
									throw ex;
								}
								if (int.Parse(words[3]) < 0 || int.Parse(words[3]) > 2)
								{
									Exception ex = new Exception("Некорректный тип груза. Тип груза должен находится в диапазоне [0,2]");
									throw ex;
								}
								if (TimeSpan.Parse(words[2]) <= new TimeSpan())
								{
									Exception ex = new Exception("Длительной разгрузки должна быть больше чем 0");
									throw ex;
								}  
                        var ship = new Ship(words[0], DateTime.Parse(words[1]), TimeSpan.Parse(words[2]), int.Parse(words[3]));
                    }
							CorrectScheduleFile = true;
							str.Close();
                }
					 using (StreamReader str = new StreamReader(ParamsFilename.Text))
					 {
							//ЧТЕНИЕ ПАРАМЕТРОВ
								line = str.ReadLine();
							 string[] words = line.Split(del, StringSplitOptions.RemoveEmptyEntries);
							 string[] cranes = words[0].Split(del2, StringSplitOptions.RemoveEmptyEntries);
							 //string[] deviationSchedule = words[1].Split(del2, StringSplitOptions.RemoveEmptyEntries);
							 portCranes[0]=int.Parse(cranes[0]);
							 portCranes[1] = int.Parse(cranes[1]);
							 portCranes[2] = int.Parse(cranes[2]);
							 if (portCranes[0] < 1 || portCranes[1] < 1 || portCranes[2] < 1) {
								 Exception ex = new Exception("Вы ввели некорректное число кранов");
								 throw ex;
							 }
							 if ((portCranes[0] + portCranes[1] + portCranes[2])>6) {
								 Exception ex = new Exception("Количество кранов не может быть больше 6");
								 throw ex;
							 }
							 if (TimeSpan.Parse(words[1]) <= new TimeSpan())
							 {
								 Exception ex = new Exception("Диапазон отклонения от обычного времени разгрузки должен быть больше 0");
								 throw ex;
							 }  
							 //portDevitationFromSchedule[0] = TimeSpan.Parse(deviationSchedule[0]);
							 //portDevitationFromSchedule[1] = TimeSpan.Parse(deviationSchedule[1]);
							 portDevitationFromUnloadTime[0] = new TimeSpan();
							 portDevitationFromUnloadTime[1] = TimeSpan.Parse(words[1]);
							 portPenalty = int.Parse(words[2]);
							 str.Close();
							CorrectParametersFile = true;
					 }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
				
				if (CorrectScheduleFile && CorrectParametersFile)
            {
                //Инизиализация графики
                ShipHandler ArrivingShip = new ShipHandler(ShowShip);
                CraneHandler CreatedCrane = new CraneHandler(ShowCrane);
                ShipMoveHandler PortShipMover = new ShipMoveHandler(MoveShip);
                ShipMoveHandler ShipSpawner = new ShipMoveHandler(SpawnShip);
                ShipHandler RemoveShip = new ShipHandler(Rotation);

                //Инициализация порта
                TimeSpan portStepSimulation = new TimeSpan(3, 0, 0, 0);
                TimeSpan portPeriodSimulation = new TimeSpan(30, 0, 0, 0);
                MyPort = Port.getInstance(portStepSimulation, portPeriodSimulation, portCranes, portDevitationFromSchedule, portDevitationFromUnloadTime, portPenalty,
                    CreatedCrane, PortShipMover, ShipSpawner, RemoveShip);
                MySchedule = Schedule.getInstance(ArrivingShip, FileNameBox.Text);
                MyJoTimer = JoTimer.getInstance();
                MyJoTimer.AddObserver(MainWindowObj);
                MyJoTimer.AddObserver(MySchedule);
					 Start.Visibility = Visibility.Hidden;
					 ParamsFilename.Visibility=Visibility.Hidden;
					 Params_lbl.Visibility = Visibility.Hidden;
					 FileNameBox.Focusable=false;
            }
		}
		void Observer.TimeUpdate() {
			TimeLabel.Dispatcher.BeginInvoke((ThreadStart) delegate() {
			    TimeLabel.Content=MyJoTimer.GetTime();
			});
		}
		void Observer.JoAction(string mes, int id) {
			if (id == 1) {
				ShipLabel.Dispatcher.BeginInvoke((ThreadStart)delegate() {
					ShipLabel.Content = mes;
				});
			}
			if (id == 2) {
				Start.Dispatcher.BeginInvoke((ThreadStart)delegate()
				{
					Start.Visibility=Visibility.Hidden;
					Stats_but.Visibility = Visibility.Visible;
					Show_schedule.Visibility = Visibility.Visible;
					History_but.Visibility = Visibility.Visible;
				});
				//событие по ид 2
			}
		}
        private void ShowShip(Ship arrivedShip)
        {
            MainCanvas.Dispatcher.BeginInvoke((ThreadStart)delegate()
            {
                var ShipCanvas = new Canvas();

                var ship = new Image();
                ship.Width = Constants.ShipWidht;
                ship.Height = Constants.ShipHeight;
                ship.Source = new BitmapImage(new Uri("Images/ship.JPG",UriKind.Relative));
                var i = new RotateTransform(180);
                ship.LayoutTransform = i;
                ship.Stretch = Stretch.Fill;
                Canvas.SetLeft(ship, 20);
                Canvas.SetTop(ship, 0);
                ShipCanvas.Children.Insert(0,ship);

                var lbl = new Label();
                Canvas.SetLeft(lbl, 0);
                Canvas.SetTop(lbl, -10);
                ShipCanvas.Children.Insert(1, lbl);
                Canvas.SetLeft(ShipCanvas, 0);
                Canvas.SetTop(ShipCanvas, 0);
                MainCanvas.Children.Add(ShipCanvas);
                arrivedShip.Outlook = ShipCanvas;
					 MyPort.AddToHistory("Корабль " + arrivedShip.Name + " прибыл (" + MyJoTimer.GetTime() + ")");
                Constants.StartShipXCoord+= 100;
            });
        }
        private void SpawnShip(Ship arrivedShip,Position pos,TimeSpan tsp)
        {
            MainCanvas.Dispatcher.BeginInvoke((ThreadStart)delegate()
            {
                var shipOutlook = arrivedShip.Outlook;
                Canvas.SetLeft(shipOutlook, pos.X);
                Canvas.SetTop(shipOutlook, pos.Y);
                var k =(Label)shipOutlook.Children[1];
                k.Content = string.Format("{0}", arrivedShip.Name,arrivedShip.CargoType);
            });
        }
        private void ShowCrane(HarborCrane createdCrane)
        {
            MainCanvas.Dispatcher.BeginInvoke((ThreadStart)delegate()
                {
                    var craneOutlook = new Canvas();

                    var crane = new Image();
                    crane.Width = 50;
                    crane.Height = 50;
                    string typeOfCrane = "Images/crane_" +
                        (createdCrane.CargoType + 1).ToString() + ".JPG";
                    crane.Source = new BitmapImage(new Uri(typeOfCrane, UriKind.Relative));
                    Canvas.SetTop(crane, 5);
                    Canvas.SetLeft(crane, 0);
                    craneOutlook.Children.Insert(0,crane);

                    var lbl = new Label();
                    //lbl.Content = string.Format("({0} {1} :{2})", createdCrane.MyPosition.X, createdCrane.MyPosition.Y,createdCrane.CargoType);
                    lbl.Content = "";
                    Canvas.SetTop(lbl, 0);
                    Canvas.SetLeft(lbl, 2);
                    craneOutlook.Children.Add(lbl);

                    Canvas.SetTop(craneOutlook, createdCrane.MyPosition.Y);
                    Canvas.SetLeft(craneOutlook, createdCrane.MyPosition.X);
                    MainCanvas.Children.Add(craneOutlook);
                    createdCrane.Outlook = craneOutlook;
                });
        }
        private void MoveShip(Ship ship,Position targetPos,TimeSpan TSpan)
        {
            MainCanvas.Dispatcher.BeginInvoke((ThreadStart)delegate()
            {
                var XFrom = Canvas.GetLeft(ship.Outlook);
                var YFrom = Canvas.GetTop(ship.Outlook);
                var Xanimation = new DoubleAnimation(XFrom,targetPos.X,TimeSpan.FromSeconds(3));
                var Yanimation = new DoubleAnimation(YFrom, targetPos.Y, TimeSpan.FromSeconds(3));
                Xanimation.BeginTime = TSpan;
                Yanimation.BeginTime = TSpan;
                ship.Outlook.BeginAnimation(Canvas.LeftProperty, Xanimation);
                ship.Outlook.BeginAnimation(Canvas.TopProperty, Yanimation);
            });
        }
        private void Rotation(Ship ship)
        {
            MainCanvas.Dispatcher.BeginInvoke((ThreadStart)delegate() {
                var animation = new DoubleAnimation(0,180, TimeSpan.FromSeconds(2));
                ship.Outlook.RenderTransform = new RotateTransform(0,0,Constants.ShipHeight/2);
                ship.Outlook.RenderTransform.BeginAnimation(RotateTransform.AngleProperty, animation);
                var lbl = (Label)ship.Outlook.Children[1];
                lbl.LayoutTransform = new RotateTransform(180);
            });

        }
		  private void Stats_but_Click(object sender, RoutedEventArgs e) {
				//Вывод статистики
				Statistics MyStats = new Statistics();
				Information_block.Content = MyStats.GetStatsInformation();
				Information_block.Visibility = Visibility.Visible;
		  }

		  private void Show_schedule_Click(object sender, RoutedEventArgs e)
		  {
				//Вывод расписания
			  Statistics MyStats = new Statistics();
			  Information_block.Content = MyStats.GetScheduleInformation();
			  Information_block.Visibility = Visibility.Visible;
		  }

		  private void History_but_Click(object sender, RoutedEventArgs e)
		  {
				string str="";
			  foreach (string line in MyPort.History){
				  str += line + "\n";
			  }
			  Information_block.Content=str;
			  Information_block.Visibility = Visibility.Visible;
		  }


        
	}
}
