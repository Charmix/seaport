﻿using System;
using System.Timers;
using System.Collections.Generic;

namespace ModelirovaniePorta
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			//Инициализация порта
			TimeSpan portStepSimulation = new TimeSpan (3,0,0,0);
			TimeSpan portPeriodSimulation = new TimeSpan (30,0,0,0);
			int[] portCranes = { 1, 1, 1 };
			double portPenalty = 10;
			TimeSpan[] portDevitationFromSchedule = { new TimeSpan (-2, 0,0,0), new TimeSpan (9, 0,0,0) }; 
			TimeSpan[] portDevitationFromUnloadTime = { new TimeSpan (), new TimeSpan (12, 0,0,0)};

			Port MyPort = Port.getInstance(portStepSimulation, portPeriodSimulation, portCranes, portDevitationFromSchedule, portDevitationFromUnloadTime, portPenalty);

			//Инициализация списка кораблей
			Schedule MySchedule = Schedule.getInstance ();
			JoTimer MyJoTimer = JoTimer.getInstance ();
			MyJoTimer.AddObserver (MySchedule);

			//Инициализация статистики
			Statistics MyStats = new Statistics ();
			while (MySchedule.AllShips.Find (x => x.Step <= 2) != default(Ship)) { 
				//Программа работает, пока все корабли не закончили разгрузку или не сработало исключение.
			} 
			foreach (string once in MyPort.History) {
				Console.WriteLine (once);
			}

			//Сюда же можешь писать свои заметки 

//			Я хочу чтобы все ошибки обрабатывались в одном месте(то есть любая ошибка в коде ловилась 
//			и выводилась в программе). try catch здесь не поможет. (не реализовано)
			// Рандомную шнягу, которая с каждым шагом моделирования влияет на расписание и длительность разгрузки (не реализовано)

		}

	}
}
