﻿using System;

namespace ModelirovaniePorta
{
	public class Ship:IComparable<Ship>
	{
		//Приватные поля
		private DateTime _ArrivalTime;
		private TimeSpan _DurationUnloadTime;
		private DateTime _StartUnloadTime;
		private DateTimeFunctions DTFunctions = new DateTimeFunctions ();
		private Random random = new Random();
		private int _CargoType;
		private string[] _CargoTypeStringArray= {"Сыпучий", "Жидкий", "Контейнеры"};

		//Доступные поля
		public string Name;
		public int CargoWeight;
		public int Step; //0 - не прибыл; 1 - прибыл, в очереди на разгрузку; 2 - разгружается; 3 - закончил разгрузку.
		public DateTime ArrivalTime {
			get { return _ArrivalTime; }
		}
		public TimeSpan DurationUnloadTime {
			get { return _DurationUnloadTime; }
		}
		public DateTime StartUnloadTime {
			get { return _StartUnloadTime; }
			set { _StartUnloadTime = value;}
		}
		public int CargoType {
			get { return _CargoType; }
		}
		public string CargoTypeString {
			get { return _CargoTypeStringArray[_CargoType]; }
		}

		//Методы
		public void AddToArrivalTime(TimeSpan time) {
			_ArrivalTime = _ArrivalTime+time;
		}
		public void UpStep() {
			this.Step++;
		}
		public void AddToDurationUnloadTime(TimeSpan time) {
			_DurationUnloadTime = _DurationUnloadTime+time;
		}
		public TimeSpan GetWaitingTime() {
			return _StartUnloadTime - _ArrivalTime;
		}
		public string ToPrint() {
			string str = "Имя корабля: " + this.Name;
			str += "\nВремя прибытия: " + this._ArrivalTime;
			str += "\nВремя ожидания: " + this.GetWaitingTime();
			str += "\nНачало разгрузки: " + this._StartUnloadTime;
			str += "\nПродолжительность разгрузки: " + this._DurationUnloadTime;
			return str;
		}
		public static bool operator <(Ship obj1, Ship obj2) {
			return (obj1.ArrivalTime < obj2.ArrivalTime);
		}
		public static bool operator >(Ship obj1, Ship obj2) {
			return (obj1.ArrivalTime > obj2.ArrivalTime);
		}
		public int CompareTo(Ship obj){
			if (this._ArrivalTime > obj._ArrivalTime)
				return 1;
			if (this._ArrivalTime < obj._ArrivalTime)
				return -1;
			else
				return 0;
		}

		//Конструкторы
		public Ship (string name, DateTime ArrivalTime, TimeSpan DurationUnloadTime, int CargoType) {
			_ArrivalTime = ArrivalTime;
			_DurationUnloadTime = DurationUnloadTime;
			Name=name;
			_CargoType = CargoType;
			Step = 0;
			CargoWeight = random.Next(2000, 5000);
		}
	}

}

