﻿using System;

namespace ModelirovaniePorta
{
	public class DateTimeFunctions
	{
		private Random random = new Random();
		private DateTime StartTime=new DateTime(2016,1,1);
		private int RangeDays=15;
		public DateTime GetRandomTimeAfterStartTime() {    
			return StartTime.AddDays(random.Next(RangeDays));
		}
		public DateTimeFunctions () {
		}
	}
}

