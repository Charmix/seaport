﻿using System;
using System.Collections.Generic;

namespace ModelirovaniePorta
{
	public class Statistics
	{
		//Приватные поля
		private Port MyPort;
		private Schedule MySchedule;

		//Методы
		public int GetTheNumberOfShips() {
			return MySchedule.AllShips.Count;
		}
		public TimeSpan GetMidWaitingTime() {
			TimeSpan midWaitingTime = new TimeSpan(GetAllWaitingTime().Ticks / MySchedule.AllShips.Count);
			return midWaitingTime;
		}
		public TimeSpan GetMaxWaitingTime() {
			TimeSpan maxWaitingTime=new TimeSpan();
			foreach (Ship element in MySchedule.AllShips) {
				if (maxWaitingTime < element.GetWaitingTime ()) {
					maxWaitingTime = element.GetWaitingTime ();
				}
			}
			return maxWaitingTime;
		}
		public int GetMidLengthOfQueue() {
			int queue_count = 0;
			foreach (HarborCrane crane in MyPort.PortCranes) {
				queue_count += crane.MaxQueue;
			}
			return Convert.ToInt32(Math.Round ((double) (queue_count / MyPort.PortCranes.Count)));
		}
		public TimeSpan GetAllWaitingTime() {
			TimeSpan allWaitingTime=new TimeSpan();
			foreach (Ship element in MySchedule.AllShips) {
				allWaitingTime = allWaitingTime+element.GetWaitingTime ();
			}
			return allWaitingTime;
		}
		public double GetTotalPenalties() {
			return GetAllWaitingTime ().Hours * MyPort.PenaltyForOneHour;
		}
			
		//Конструктор
		public Statistics () {
			MySchedule = Schedule.getInstance ();
			MyPort = Port.getInstance ();
		}
	}
}

