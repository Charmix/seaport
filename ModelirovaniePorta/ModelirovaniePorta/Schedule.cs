﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Timers;

namespace ModelirovaniePorta
{
	public class Schedule:Observer
	{
		//Поля
		public List<Ship> AllShips = new List<Ship> (); //Только здесь хранится весь список кораблей 
		private JoTimer MyJoTimer;
		private Port MyPort;
		private static Schedule uniqueInstance;

		//Методы
		private void Sort() {
			AllShips.Sort ();
		}
		public override void TimeUpdate() {
			//Время обновилось
			Ship cur_ship;
			//Сделал циклом, чтобы можно было реализовать пребытие кораблей в одной время.
			while ((cur_ship = AllShips.Find (x => x.Step == 0)) != default(Ship) && MyJoTimer.CheckThisTime (cur_ship.ArrivalTime)) {
					ShipArrive (cur_ship);
			}
		}
		public void ShipArrive(Ship arrivedShip) {
			arrivedShip.UpStep ();
			MyPort.AddToHistory ("Корабль "+arrivedShip.Name+" прибыл. ( " +MyJoTimer.GetTime()+" )");
			MyPort.AddArrivedShipToCrane(arrivedShip);
			PrintStepsOfAllShips ();
        }
		public void PrintStepsOfAllShips() {
			foreach (Ship obj in AllShips) {
				Console.WriteLine (obj.Name + "" + obj.Step);
			}
		}
		//Данный метод используется только при создании
		public static Schedule getInstance() {
			if (uniqueInstance == null)
				uniqueInstance = new Schedule();
			return uniqueInstance;
		}
			
		//Конструктор
		private Schedule (){
			Ship curship;
			MyPort = Port.getInstance (); //Получили объект порта(паттерн Одиночка)
			string filename="schedule.txt";
			string line = "";
			string[] del = new string[]{ " @ " };
			try {
				using (StreamReader str = new StreamReader (filename)) {
					while((line = str.ReadLine()) != null) {
						// Килт @ 14.11.2016 6:50:30 @ 01:02:50:00 @ 1
						string[] words = line.Split (del, StringSplitOptions.RemoveEmptyEntries);
						AllShips.Add(new Ship (words[0], DateTime.Parse(words[1]),  TimeSpan.Parse(words[2]), int.Parse(words[3])));
					}
				}
			}
			catch (Exception e) {
				Console.WriteLine (e.Message);
			}

			AllShips.Sort ();
			MyJoTimer = JoTimer.getInstance (AllShips [0].ArrivalTime.AddMinutes (-1), new List<Observer>(MyPort.PortCranes));
			MyJoTimer.Start ();
		}
	}
}

