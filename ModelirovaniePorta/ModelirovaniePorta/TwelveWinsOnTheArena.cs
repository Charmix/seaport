﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModelirovaniePorta
{
    class TwelveWinsOnTheArena
    {
        private string name;
        private int wins;

        public string Name
        {
            get
            {
                return name;
            }
        }
        public int Wins
        {
            get
            {
                return wins;
            }
        }
        
        public void GetWinsByName()
        {
            Random rnd = new Random();
            if (Name == "Лапшин")
                wins = 12;
            else
                wins = rnd.Next(0,4);
        }
        public void ToPrint()
        {
            Console.WriteLine(name+wins);
        }

        public TwelveWinsOnTheArena(string name)
        {
            this.name = name;
        }
    }
}
