﻿using System;
using System.Collections.Generic;

namespace ModelirovaniePorta
{
	public class Port
	{
		//Приватные поля
		private int[] _NumberOfCranes;
		private TimeSpan[] _DeviationFromSchedule;
		private TimeSpan[] _DeviationFromUnloadTime;
		private TimeSpan _StepSimulation;
        private TimeSpan _PeriodSimulation;
        private List<HarborCrane> _PortCranes;
		private List<String> _History;
		private Schedule MySchedule;

        //Поле хранящее экземпляр Port
        private static Port uniqueInstance;

		//Доступные поля
		public double PenaltyForOneHour;
		public List<String> History {
			get { return _History; }
		}
		public int[] NumberOfCranes {
			get { return _NumberOfCranes; }
		}
		public TimeSpan[] DeviationFromSchedule {
			get { return _DeviationFromSchedule; }
		}
		public TimeSpan[] DeviationFromUnloadTime {
			get { return _DeviationFromUnloadTime; }
		}
		public TimeSpan StepSimulation {
			get { return _StepSimulation; }
		}
		public TimeSpan PeriodSimulation {
			get { return _PeriodSimulation; }
		}
		public List<HarborCrane> PortCranes {
            get { return _PortCranes; }
        }

		//Методы
		public List<HarborCrane> CreateCranes(int[] numberOfCranes) {
            List<HarborCrane> cranes = new List<HarborCrane>();
            for (int i = 0; i < numberOfCranes.Length; i++)
                for (int j = 0; j < numberOfCranes[i]; j++)
                    cranes.Add(new HarborCrane(i));
            return cranes;
        }
		public void AddArrivedShipToCrane(Ship arrivedShip) {
            HarborCrane craneForShip = FindCrane(arrivedShip.CargoType);
			craneForShip.AddShipToQueue(arrivedShip);
			AddToHistory (craneForShip.ToPrintQueue());
        }
		public HarborCrane FindCrane(int typeOfCargo) {
            HarborCrane freeCrane = PortCranes[0];
            TimeSpan freeCraneTime = freeCrane.UploadQueueTime();
            for (int i = 1; i < PortCranes.Count; i++)
                if (PortCranes[i].CargoType == typeOfCargo){
                    TimeSpan craneTime = PortCranes[i].UploadQueueTime();
					if (craneTime < freeCraneTime) {
                        freeCrane = PortCranes[i];
                        freeCraneTime = freeCrane.UploadQueueTime();
                    }
                }
            return freeCrane;
        }
		public void AddToHistory(string Message) {
			_History.Add (Message);
			MySchedule = Schedule.getInstance ();
			//MySchedule.PrintStepsOfAllShips ();
			Console.WriteLine (Message);
		}
		/*public void ReceiveException (Exception e) {
			Console.WriteLine("Ошибка во время выполнения программы: {0}", e.Message);  
			Environment.Exit (0);
		}*/

        //Методы создания экземпляра Port
		public static Port getInstance() {
            return uniqueInstance;
        }

        //Данный метод используется только при создании
		public static Port getInstance(TimeSpan stepSimulation, TimeSpan periodSimulation, int[] Cranes, TimeSpan[] DeviationFromSchedule, TimeSpan[] DeviationFromUnloadTime, double PortPenalty) {
            if (uniqueInstance == null)
				uniqueInstance = new Port(stepSimulation, periodSimulation, Cranes, DeviationFromSchedule, DeviationFromUnloadTime, PortPenalty);
            return uniqueInstance;
        }

		//Конструктор (Приватный для Одиночки)
		private Port (TimeSpan stepSimulation, TimeSpan periodSimulation, int[] Cranes, TimeSpan[] DeviationFromSchedule, TimeSpan[] DeviationFromUnloadTime, double PortPenalty) {
			_StepSimulation = stepSimulation;
			_PeriodSimulation = periodSimulation;
			_NumberOfCranes = Cranes;
			_DeviationFromSchedule = DeviationFromSchedule;
			_DeviationFromUnloadTime = DeviationFromUnloadTime;
            _PortCranes = CreateCranes(_NumberOfCranes);
			PenaltyForOneHour = PortPenalty;
			_History = new List<String> ();
		}
	}
}

