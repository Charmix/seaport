﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Timers;

namespace ModelirovaniePorta
{
	public class JoTimer
	{
		//Поля
		private List<Observer> _Observers;
		private Timer _Timer;
		private int _TimerInterval=3; //в мс
		private double SpeedOfTimer=1; //в секундах
		private DateTime _CurrentTime;

		private static JoTimer uniqueInstance;

		//Методы
		private void TimerInterval(object source, ElapsedEventArgs e) {
			_CurrentTime = _CurrentTime.AddSeconds(SpeedOfTimer);
			foreach (Observer observer in _Observers) { //Уведомляем наблюдателей, что время поменялось
				observer.TimeUpdate ();
			}
			Console.WriteLine("Текущее время: "+GetTime());
		}
		public DateTime GetTime() {
			return _CurrentTime;
		}
		public void Stop() {
			_Timer.Stop ();
		}
		public void Start() {
			_Timer.Start ();
		}
		public void AddObserver(Observer obj) {
			_Observers.Add (obj);
		}
		public void RemoveObserver(Observer obj) {
			_Observers.Remove (obj);
		}
		public static JoTimer getInstance() {
			return uniqueInstance;
		}
		public TimeSpan HowMuchTimeBeforeThisTime(DateTime obj) {
			return  obj-_CurrentTime;
		}
		public bool CheckThisTime(DateTime obj) {
			if (HowMuchTimeBeforeThisTime (obj).TotalSeconds == 0) {
				return true;
			} else {
				return false;
			}
		}

		//Данный метод используется только при создании
		public static JoTimer getInstance(DateTime startDateTime, List<Observer> observers) {
			if (uniqueInstance == null)
				uniqueInstance = new JoTimer(startDateTime, observers);
			return uniqueInstance;
		}

		//Конструктор
		private JoTimer (DateTime startDateTime, List<Observer> observers) {
			_Observers = observers;
			_Timer = new Timer ();
			_Timer.Elapsed += new ElapsedEventHandler(TimerInterval);
			_Timer.Interval = _TimerInterval;
			_CurrentTime = startDateTime;
		}

	}
}

