﻿using System;

namespace ModelirovaniePorta
{
	public abstract class Observer
	{
		public abstract void TimeUpdate ();
	}
}

