﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModelirovaniePorta
{
	public class HarborCrane:Observer
    {
		//Поля
        private int _CargoType;
		private string _CraneId;
		private Queue<Ship> _UnloadQueue; 
		private int _MaxQueue=0;
		private Ship _UnloadShip;
		private JoTimer MyJoTimer;
		private Port MyPort;
		public int CargoType {
            get { return _CargoType; }
        }
		public int MaxQueue {
			get { return _MaxQueue; }
		}
		public Ship UnloadShip {
			get { return _UnloadShip; }
		}

		//Методы
		public string ToPrintQueue() {
			string result = "Очередь крана "+_CraneId+": ";
			foreach (var ship in _UnloadQueue) {
				result += ship.Name+" ";
            }
            return result;
        }
		public void AddShipToQueue(Ship ship) {
			_UnloadQueue.Enqueue (ship);
			if (_UnloadQueue.Count > _MaxQueue && _UnloadShip != null) {
				_MaxQueue = _UnloadQueue.Count;
			}
        }
		public TimeSpan UploadQueueTime() {
            TimeSpan tspan = new TimeSpan();
            foreach (var ship in _UnloadQueue)
                tspan += ship.DurationUnloadTime;
            return tspan;
        }
		public string generateId() {
			return Guid.NewGuid().ToString("N");
		}
		public override void TimeUpdate() {
			MyPort = Port.getInstance ();
			MyJoTimer = JoTimer.getInstance ();
			//Время обновилось
			if (_UnloadShip == null) {
				if (_UnloadQueue.Count != 0) {
					//Если очередь на разгрузку не пуста и нету текущего разгружаемого корабля, то отправляем его на разгрузку.
					_UnloadShip = _UnloadQueue.Dequeue ();
					_UnloadShip.UpStep ();
					_UnloadShip.StartUnloadTime = MyJoTimer.GetTime ();
				}
			} else {
				if (_UnloadShip.StartUnloadTime.Add(_UnloadShip.DurationUnloadTime) == MyJoTimer.GetTime ()) {
					MyPort.AddToHistory ("Корабль "+_UnloadShip.Name+" закончил разгрузку у крана "+this._CraneId+". ( " +MyJoTimer.GetTime()+" )");
					_UnloadShip.UpStep ();		
					_UnloadShip = null;
				}
			}
		}

		//Конструкторы
		public HarborCrane(int cargoType) {
            _CargoType = cargoType;
			_UnloadQueue = new Queue<Ship>();
			_CraneId = generateId ();
			_UnloadShip = null;
        }
    }
}
